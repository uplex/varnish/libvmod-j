/*-
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <cache/cache.h>

#include "vsbjson.h"
#include "vcc_j_if.h"

static int
is_jnumber(const char *s)
{
	if (*s == '-')
		s++;

	// 0 or [1-9][0-9]*
	if (*s == '0')
		s++;
	else if (! (*s >= '1' && *s <= '9'))
		return (0);
	else {
		// *s == 1..9
		s++;
		while (*s >= '0' && *s <= '9')
			s++;
	}
	// fraction
	if (*s == '.') {
		s++;
		while (*s >= '0' && *s <= '9')
			s++;
	}
	// exponent
	if (*s == 'e' || *s == 'E') {
		s++;
		if (*s == '-' || *s == '+')
			s++;
		while (*s >= '0' && *s <= '9')
			s++;
	}
	if (*s != '\0')
		return (0);
	return (1);
}

static int
is_jlit(const char *s)
{
	return (strcmp(s, "true") == 0 ||
		strcmp(s, "false") == 0 ||
		strcmp(s, "null") == 0);
}

// magic values
#define JM_object	0x81
#define JM_min JM_object
#define JM_array	0x82
#define JM_string	0x83
#define JM_number	0x84
#define JM_lit		0x85
#define JM_max JM_lit

#define AL __attribute__((aligned(2)))
static const unsigned char jm_object[2] AL = { JM_object, '{' };
static const unsigned char jm_array[2]  AL = { JM_array,  '[' };
static const unsigned char jm_string[2] AL = { JM_string, '"' };

static const unsigned char jnil_object[4] AL = { JM_object, '{','}', 0 };
static const unsigned char jnil_array[4]  AL = { JM_array,  '[',']', 0 };
static const unsigned char jnil_string[4] AL = { JM_string, '"','"', 0 };
static const unsigned char jnull[6] AL =  { JM_lit, 'n','u','l','l', 0};
static const unsigned char jtrue[6] AL =  { JM_lit, 't','r','u','e', 0};
static const unsigned char jfalse[7] AL = { JM_lit, 'f','a','l','s','e', 0};

#define VSB_mcat(vsb, x) (void)VSB_bcat(vsb, x, sizeof(x))

#define isJ(p, X) (((uintptr_t)(p) & 1) == 1 &&				\
	memcmp((unsigned char *)(p) - 1, jm_ ## X, sizeof(jm_ ## X)) == 0)
#define is_Jobject(p) isJ(p, object)
#define is_Jarray(p)  isJ(p, array)
#define is_Jstring(p) isJ(p, string)

// is this specific enough?
#define is_J(p) ((uintptr_t)(p) & 1 &&			\
	(unsigned char)(p)[-1] >= JM_min &&		\
	(unsigned char)(p)[-1] <= JM_max)

static void __attribute__((constructor))
assert_statics(void)
{
	//lint --e{506}
	assert(is_Jobject(jm_object + 1));
	assert(is_Jobject(jnil_object + 1));
	assert(is_Jarray(jm_array + 1));
	assert(is_Jarray(jnil_array + 1));
	assert(is_Jstring(jm_string + 1));
	assert(is_J(jnull + 1));
	assert(is_J(jtrue + 1));
	assert(is_J(jfalse + 1));
}

static inline int
is_Jnumber(const char *p)
{

	if (((uintptr_t)p & 1) == 0)
		return (0);
	if ((unsigned char)p[-1] != JM_number)
		return (0);
	if (*p == '-')
		p++;
	return (*p >= '0' && *p <= '9');
}

VCL_STRANDS
vmod_nil(VRT_CTX)
{
	(void) ctx;
	return (vrt_null_strands);
}

VCL_STRING
vmod_null(VRT_CTX)
{
	(void) ctx;
	return ((const char *)jnull + 1);
}

VCL_STRING
vmod_true(VRT_CTX)
{
	(void) ctx;
	return ((const char *)jtrue + 1);
}

VCL_STRING
vmod_false(VRT_CTX)
{
	(void) ctx;
	return ((const char *)jfalse + 1);
}

#define ints_sz (size_t)4
#define ints_lim (int)100
static char ints[ints_lim][ints_sz] AL;

static void __attribute__((constructor))
init_ints(void)
{
	size_t u;
	int r;

	for (u = 0; u < ints_lim; u++) {
		r = snprintf(ints[u], ints_sz, "%c%zu", JM_number, u);
		assert((unsigned)r < ints_sz);
		assert(is_Jnumber(ints[u] + 1));
	}
}

VCL_STRING
vmod_number(VRT_CTX, struct VARGS(number)*a)
{
	struct vsb vsb[1];
	const char *p;
	int valid_i, valid_d;
	intmax_t i = 0;
	double d = nan("");

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(a);
	(void)a;

	valid_i = a->valid_integer + a->valid_bytes;
	valid_d = a->valid_real + a->valid_duration + a->valid_time;

	if (valid_i  + valid_d + a->valid_string != 1) {
		VRT_fail(ctx, "j.number(): exactly one argument must be given");
		return (NULL);
	}

	if (a->valid_string) {
		if (! is_jnumber(a->string)) {
			VRT_fail(ctx, "j.number(): string %s invalid" ,
			    a->string);
			return (NULL);
		}
	}
	else if (a->valid_integer)
		i = a->integer;
	else if (a->valid_bytes)
		i = a->bytes;
	else if (a->valid_real)
		d = a->real;
	else if (a->valid_duration)
		d = a->duration;
	else if (a->valid_time)
		d = a->time;
	else
		WRONG("args invalid");

	if (valid_i && ! VRT_INT_is_valid(i)) {
		VRT_fail(ctx, "INT overflow converting to string (0x%jx)", i);
		return (NULL);
	}
	else if (valid_d && ! VRT_REAL_is_valid(d)) {
		VRT_fail(ctx, "REAL overflow converting to string (%e)", d);
		return (NULL);
	}

	if (valid_i && i >= 0 && i < ints_lim)
		return (ints[i] + 1);

	// on %.15g: https://stackoverflow.com/questions/30658919/the-precision-of-printf-with-specifier-g/54162486#54162486

	WS_VSB_new(vsb, ctx->ws);
	(void) VSB_putc(vsb, JM_number);
	if (a->valid_string)
		(void)VSB_bcat(vsb, a->string, (ssize_t)strlen(a->string));
	else if (valid_i)
		(void)VSB_printf(vsb, "%jd", i);
	else if (valid_d)
		(void)VSB_printf(vsb, "%.15g", d);
	else
		WRONG("valid_X");
	p = WS_VSB_finish(vsb, ctx->ws, NULL);
	if (p == NULL) {
		VRT_fail(ctx, "j.number(): out of workspace");
		return (NULL);
	}
	AZ((uintptr_t)p & 1);
	return (p + 1);
}

static void
vsbjstring(struct vsb *vsb, const char *pa)
{

	(void) VSB_putc(vsb, '"');
	(void) vsbjminimal(vsb, pa);
	(void) VSB_putc(vsb, '"');
}

VCL_STRING
vmod_string(VRT_CTX, VCL_STRANDS s, VCL_ENUM esc)
{
	struct strands sc;
	struct vsb vsb[1];
	const char *p, *e;
	ssize_t l;
	int i;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(s);

	// find first character
	for (i = 0; i < s->n; i++) {
		if (s->p[i] != NULL && *s->p[i] != '\0')
			break;
	}
	if (i == s->n)
		return ((const char *)jnil_string + 1);

	const char *scp[s->n];
	memset(scp, 0, sizeof scp);
	sc.n = 0;
	sc.p = scp;

	if (esc == VENUM(none)) {
		// skip initial quote
		if (strcmp(s->p[i], "\"") == 0) {
			i++;
			if (i == s->n)
				return ((const char *)jnil_string + 1);
		}
		else if (s->p[i][0] == '"')
			sc.p[sc.n++] = s->p[i++] + 1;
	}

	for (; i < s->n; i++)
		if (s->p[i] != NULL && *(s->p[i]) != '\0')
			sc.p[sc.n++] = s->p[i];

	WS_VSB_new(vsb, ctx->ws);
	VSB_mcat(vsb, jm_string);	// "

	if (esc == VENUM(none)) {
		for (i = 0; i < sc.n - 1; i++)
			(void)VSB_bcat(vsb, sc.p[i], (ssize_t)strlen(sc.p[i]));
		assert(i == sc.n - 1);
		l = (ssize_t)strlen(sc.p[i]);
		AN(l);
		if (sc.p[i][l - 1] == '"')
			l--;
		(void)VSB_bcat(vsb, sc.p[i], l);
	}
	else if (esc == VENUM(minimal)) {
		for (i = 0; i < sc.n; i++)
			if (! vsbjminimal(vsb, sc.p[i]))
				break;
	}
	else if (esc == VENUM(ascii)) {
		e = NULL;
		for (i = 0; i < sc.n; i++)
			if (! vsbjascii(vsb, sc.p[i], &e))
				break;
		if (e != NULL) {
			VRT_fail(ctx,
			    "j.string(x, ascii) bad UTF-8 at: ...%.10s", e);
			WS_Release(ctx->ws, 0);
			return (NULL);
		}
	}
	else
		WRONG("esc enum");

	(void)VSB_putc(vsb, '"');
	l = VSB_len(vsb);
	p = WS_VSB_finish(vsb, ctx->ws, NULL);
	if (p == NULL) {
		VRT_fail(ctx, "j.string(): out of workspace");
		return (NULL);
	}

	e = NULL;
	AZ(is_jquot(p + 2, &e));
	// validation succeeds if error points to the final "
	if (e == p + l - 1)
		assert(*e == '"');
	else if (esc == VENUM(none)) {
		VRT_fail(ctx,
		    "j.string(x, none) malformed string at: ...%.10s", e);
		return (NULL);
	}
	else
		WRONG("is_jquot on our own output failed");

	AZ((uintptr_t)p & 1);
	return (p + 1);
}

// add a json value to the vsb:
// - json, number or literal as is
// - anything else as string
static void
vsbjvalue(struct vsb *vsb, const char *p)
{
	if (is_J(p) || is_jnumber(p) || is_jlit(p))
		(void) VSB_bcat(vsb, p, (ssize_t)strlen(p));
	else
		vsbjstring(vsb, p);
}

VCL_STRING
vmod_array(VRT_CTX, VCL_STRANDS s)
{
	struct vsb vsb[1];
	const char *p;
	int i;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(s);

	if (s->n == 0 || s->n == 1 && *s->p[0] == '\0' && !is_J(s->p[0]))
		return ((const char *)jnil_array + 1);

	for (i = 0; i < s->n; i++) {
		if (s->p[i] != NULL)
			continue;
		VRT_fail(ctx, "Strand %d is NULL in j.array()", i);
		return (NULL);
	}

	WS_VSB_new(vsb, ctx->ws);
	VSB_mcat(vsb, jm_array);	// [
	vsbjvalue(vsb, s->p[0]);
	for (i = 1; i < s->n; i++) {
		(void)VSB_putc(vsb, ',');
		vsbjvalue(vsb, s->p[i]);
	}
	(void)VSB_putc(vsb, ']');
	p = WS_VSB_finish(vsb, ctx->ws, NULL);
	if (p == NULL) {
		VRT_fail(ctx, "j.array(): out of workspace");
		return (NULL);
	}
	AZ((uintptr_t)p & 1);
	return (p + 1);
}

// add a json key:
// - json string as is
// - other json, number or literal fails
// - anything else as string

static int
vsbjkey(VRT_CTX, struct vsb *vsb, const char *p)
{
	if (is_Jstring(p))
		(void) VSB_bcat(vsb, p, (ssize_t)strlen(p));
	else if (is_J(p) || is_jnumber(p) || is_jlit(p)) {
		VRT_fail(ctx, "keys must be strings, got %s", p);
		return (0);
	}
	else
		vsbjstring(vsb, p);
	if (! VSB_error(vsb))
		return (1);

	VRT_fail(ctx, "Out of workspace while formatting json key");
	return (0);
}

// add key: value
static int
vsbjkv(VRT_CTX,  struct vsb *vsb, const char *k, const char *v)
{
	if (! vsbjkey(ctx, vsb, k))
		return (0);
	(void)VSB_putc(vsb, ':');
	vsbjvalue(vsb, v);
	return (1);
}

VCL_STRING
vmod_object(VRT_CTX, VCL_STRANDS s)
{
	struct vsb vsb[1];
	const char *p;
	int i;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(s);

	if (s->n == 0 || s->n == 1 && *s->p[0] == '\0' && !is_J(s->p[0]))
		return ((const char *)jnil_object + 1);

	if (s->n & 1) {
		VRT_fail(ctx, "j.object() needs an even number of strands");
		return (NULL);
	}

	for (i = 0; i < s->n; i++) {
		if (s->p[i] != NULL)
			continue;
		VRT_fail(ctx, "Strand %d is NULL in j.object()", i);
		return (NULL);
	}

	assert(s->n >= 2);

	WS_VSB_new(vsb, ctx->ws);
	VSB_mcat(vsb, jm_object);	// {
	if (! vsbjkv(ctx, vsb, s->p[0], s->p[1])) {
		WS_Release(ctx->ws, 0);
		return (NULL);
	}
	for (i = 2; i < s->n; i += 2) {
		assert((i & 1) == 0);
		(void)VSB_putc(vsb, ',');
		//lint -e{679} trunc
		if (vsbjkv(ctx, vsb, s->p[i], s->p[i + 1]))
			continue;
		WS_Release(ctx->ws, 0);
		return (NULL);
	}
	(void)VSB_putc(vsb, '}');
	p = WS_VSB_finish(vsb, ctx->ws, NULL);
	if (p == NULL) {
		VRT_fail(ctx, "j.object(): out of workspace");
		return (NULL);
	}
	AZ((uintptr_t)p & 1);
	return (p + 1);
}

#define fail(ctx, fallback, ...) do {					\
		if (fallback != NULL)					\
			VSLb(ctx->vsl, SLT_Error, __VA_ARGS__);	\
		else							\
			VRT_fail(ctx, __VA_ARGS__);			\
	} while(0)

VCL_STRING
vmod_unescape(VRT_CTX, VCL_STRING p, VCL_STRING fallback)
{
	struct vsb vsb[1];
	const char *err = NULL;
	size_t l;
	char *r;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (*p == '"')
		p++;

	WS_VSB_new(vsb, ctx->ws);
	if (! vsbjunquot(vsb, p, &err) && err != NULL) {
		WS_Release(ctx->ws, 0);
		fail(ctx, fallback, "j.unescape() error at: ...%.10s", err);
		return (fallback);
	}
	r = WS_VSB_finish(vsb, ctx->ws, NULL);
	if (r == NULL) {
		VRT_fail(ctx, "j.unescape(): out of workspace");
		return (NULL);
	}
	l = strlen(r);
	if (l == 0)
		return (r);

	l--;
	if (r[l] == '"')
		r[l] = '\0';
	return (r);
}
