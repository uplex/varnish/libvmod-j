/*-
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <string.h>
#include <stddef.h>
#include <sys/types.h>

#include <vdef.h>
#include <vas.h>

#include "vsbjson.h"

#include "hoehrmann-utf8/src/hoehrmann-utf8.h"

/* add json string
 *
 * XXX various UTF-8 cases still need handling
 * - overlong
 * - use of surrogate
 * ...
 *
 * quotes:
 * "  \  / \b \f \n \r \t
 * 22 5c 2f
 */

/* <0x80 values needing quoting */
static uint8_t jquot[256];
static uint8_t junquot[256];
static void __attribute__((constructor))
init_jquot(void)
{
	memset(jquot, 0, sizeof(jquot));
	jquot['"'] = '"';
	jquot['\\'] = '\\';
//	jquot['/'] = '/';
	jquot['\b'] = 'b';
	jquot['\f'] = 'f';
	jquot['\n'] = 'n';
	jquot['\r'] = 'r';
	jquot['\t'] = 't';

	memset(junquot, 0, sizeof(junquot));
	junquot['"'] = '"';
	junquot['\\'] = '\\';
	junquot['/'] = '/';
	junquot['b'] = '\b';
	junquot['f'] = '\f';
	junquot['n'] = '\n';
	junquot['r'] = '\r';
	junquot['t'] = '\t';
}

static inline int
jerr(const uint8_t *p, const char **err)
{
	if (err)
		*err = (const char *)p;
	return (0);
}

struct unhex16_r {
	uint32_t	value;
	unsigned	len;
};

static inline struct unhex16_r
unhex16(const uint8_t *p)
{
	struct unhex16_r r;
	unsigned u;
	uint8_t s, t;

	r.value = 0;
	for (u = 0, s = 12; u < 4; u++, s -= 4) {
		if (p[u] >= '0' && p[u] <= '9')
			t = p[u] - '0';
		else if (p[u] >= 'a' && p[u] <= 'f')
			t = 0xa + p[u] - 'a';
		else if (p[u] >= 'A' && p[u] <= 'F')
			t = 0xa + p[u] - 'A';
		else
			break; // XXX illegal hex;
		assert(s <= 12);
		r.value |= (uint32_t)t << s;
	}
	r.len = u;
	return (r);
}

// JSON to utf-8
int
vsbjunquot(struct vsb *vsb, const char *pa, const char **err)
{
	const uint8_t *p = (const uint8_t *)pa;
	struct unhex16_r hex;
	const uint8_t *s;
	uint32_t code;
	uint8_t t[4];

	while (*p) {
		s = p;
		while (*p && *p != '\\')
			p++;
		if (s != p && VSB_bcat(vsb, s, p - s))
			return (0);
		if (! *p)
			return (1);
		assert(*p == '\\');
		p++;
		if (junquot[*p]) {
			if (VSB_putc(vsb, junquot[*p]))
				return (0);
			p++;
			continue;
		}
		if (*p != 'u')
			return (jerr(p, err));
		p++;

		hex = unhex16(p);
		p += hex.len;
		if (hex.len != 4)
			return (jerr(p, err));

		if (hex.value >= 0xd800 && hex.value < 0xdc00) {
			code = (hex.value & ((1<<10) - 1)) << 10;

			if (*p != '\\')
				return (jerr(p, err));
			p++;
			if (*p != 'u')
				return (jerr(p, err));
			p++;

			hex = unhex16(p);
			p += hex.len;
			if (hex.len != 4)
				return (jerr(p, err));

			if (hex.value < 0xdc00 || hex.value > 0xdfff)
				return (jerr(p, err));

			code |= hex.value & ((1<<10) - 1);
			code += 0x10000;
		}
		else
			code = hex.value;


		if (code < 0x80) {
			if (VSB_putc(vsb, (uint8_t)code))
				return (0);
		}
		else if (code < 0x800) {
			t[0] = 0xc0 | ((code >> 6) & ((1<<5) - 1));
			t[1] = 0x80 |  (code       & ((1<<6) - 1));
			if (VSB_bcat(vsb, t, 2))
				return (0);
		}
		else if (code >= 0xd800 && code <= 0xdfff)
			return (jerr(p, err));
		else if (code < 0x10000) {
			t[0] = 0xe0 | ((code >> 12) & ((1<<4) - 1));
			t[1] = 0x80 | ((code >>  6) & ((1<<6) - 1));
			t[2] = 0x80 |  (code        & ((1<<6) - 1));
			if (VSB_bcat(vsb, t, 3))
				return (0);
		}
		else if (code < 0x110000) {
			t[0] = 0xf0 | ((code >> 18) & ((1<<3) - 1));
			t[1] = 0x80 | ((code >> 12) & ((1<<6) - 1));
			t[2] = 0x80 | ((code >>  6) & ((1<<6) - 1));
			t[3] = 0x80 |  (code        & ((1<<6) - 1));
			if (VSB_bcat(vsb, t, 4))
				return (0);
		}
		else
			WRONG("impossible high code point");
	}
	return (1);
}

// json-encode characters below 0x80
static inline int
vsbjx80(struct vsb *vsb, const uint8_t c)
{
	if (c <= 0x7e && jquot[c]) {
		(void)VSB_putc(vsb, '\\');
		(void)VSB_putc(vsb, jquot[c]);
		return (1);
	}
	if (c < 0x20) {
		(void)VSB_printf(vsb, "\\u%04x", c);
		return (1);
	}
	return (0);
}

/* utf-8 to 7-bit / ASCII JSON
 *
 * returns 0 for VSB or UTF error
 * for UTF error, *err gets set
 */
int
vsbjascii(struct vsb *vsb, const char *pa, const char **err)
{
	const uint8_t *p = (const uint8_t *)pa;
	const uint8_t *s;
	uint32_t state = 0, code;

	while (*p) {
		s = p;
		while (*p <= 0x7e && *p >= 0x20 && jquot[*p] == 0)
			p++;
		if (s != p && VSB_bcat(vsb, s, p - s))
			return (0);
		if (! *p)
			break;

		if (vsbjx80(vsb, *p)) {
			// no error checking in vsbjx80
			if (VSB_error(vsb))
				return (0);
			p++;
			continue;
		}
		assert(*p >= 0x80);

		state = 0;
		while (*p) {
			switch (utf8_decode(&state, &code, *p++)) {
			case UTF8_REJECT: {
				if (err != NULL)
					*err = (const char *)p - 1;
				return (0);
			}
			case UTF8_ACCEPT:
				/* we do not decode for < 0x80 and
				 * overlong codes are rejected
				 */
				assert(code >= 0x80);
				assert(code < 0x110000);
				if (code < 0x10000) {
					if (VSB_printf(vsb, "\\u%04x", code))
						return (0);
					break;
				}
				code -=  0x10000;
				if (VSB_printf(vsb, "\\u%04x\\u%04x",
					0xd800 | (code >> 10),
					0xdc00 | (code & ((1<<10) - 1))))
					return (0);
				break;
			default:
				continue;
			}
			break;
		}
	}
	return (1);
}

// minimal JSON quoting
int
vsbjminimal(struct vsb *vsb, const char *pa)
{
	const uint8_t *p = (const uint8_t *)pa;
	const uint8_t *s;

	while (*p) {
		s = p;
		while (*p > 0x7e || (*p >= 0x20 && jquot[*p] == 0))
			p++;
		if (s != p && VSB_bcat(vsb, s, p - s))
			return (0);
		if (! *p)
			break;
		AN(vsbjx80(vsb, *p));
		if (VSB_error(vsb))
			return (0);
		p++;
	}
	return (1);
}

int
is_jquot(const char *pa, const char **err)
{
	const uint8_t *p = (uint8_t *)pa;
	unsigned u;

	//lint -e{850}	// loop variable modified in body
	for (; *p; p++) {
		if (*p < 0x20 || *p == '"')
			return (jerr(p, err));
		if (*p != '\\')
			continue;

		p++;
		switch (*p) {
		case '"':
		case '\\':
		case '/':
		case 'b':
		case 'f':
		case 'n':
		case 'r':
		case 't':
			continue;
		case 'u':
			break;
		default:
			return (jerr(p, err));
		}
		assert(*p == 'u');
		for (u = 0; u < 4; u++) {
			p++;
			if (! ((*p >= '0' && *p <= '9') ||
			       (*p >= 'a' && *p <= 'f') ||
			       (*p >= 'A' && *p <= 'F')))
				return (jerr(p, err));
		}
	}
	return (1);
}
