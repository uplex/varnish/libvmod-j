/*-
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <vdef.h>
#include <vas.h>
#include <assert.h>

#include "vsbjson.h"

#define notquot(s, o) do {				\
	assert(is_jquot(s, &err) == 0);		\
	assert(err == s + o);				\
} while(0)

static void
t_is_jquot(void)
{
	const char *err = NULL;

	assert(is_jquot("", NULL));
	assert(is_jquot("a", NULL));
	assert(is_jquot("a€", NULL));
	assert(is_jquot("🐰", NULL));

	notquot("\x01", 0);
	notquot("a\x1f", 1);

	assert(is_jquot("xx\\\"", NULL));
	assert(is_jquot("xx\\ta", NULL));
	assert(is_jquot("x\\t\\ub33Fx\\u0000yy", NULL));
	assert(is_jquot("\\uAFFE\\u1234", NULL));
	notquot("\\", 1);
	notquot("\\x", 1);
	notquot("\\u", 2);
	notquot("\\us", 2);
	notquot("\\ua", 3);
	notquot("\\uab", 4);
	notquot("\\uabc", 5);
	notquot("\\u", 2);
	assert(is_jquot("x\\t\\ub33F", NULL));
	assert(is_jquot("x\\t\\ub33Fx", NULL));
}

static void
junquot(const char *from, const char *want)
{
	struct vsb *vsb, vsbs[1];
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AN(vsbjunquot(vsb, from, NULL));
	AZ(VSB_finish(vsb));
	if (strcmp(want, VSB_data(vsb))) {
		printf("want: \"%s\" have: \"%s\"\n",
		    want, VSB_data(vsb));
		WRONG("unquote");
	}
	VSB_fini(vsb);
}

static void
junquotoverflow(const char *from)
{
	struct vsb *vsb, vsbs[1];
	const char *err = NULL;
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AZ(vsbjunquot(vsb, from, &err));
	AN(VSB_finish(vsb));
	VSB_fini(vsb);
	AZ(err);
}

static void
junquoterr(const char *from, unsigned o)
{
	struct vsb *vsb, vsbs[1];
	const char *err = NULL;
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AZ(vsbjunquot(vsb, from, &err));
	AZ(VSB_finish(vsb));
	VSB_fini(vsb);
	assert(err == from + o);
}

static void
jascii(const char *from, const char *want)
{
	struct vsb *vsb, vsbs[1];
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AN(vsbjascii(vsb, from, NULL));
	AZ(VSB_finish(vsb));
	if (strcmp(want, VSB_data(vsb))) {
		printf("want: \"%s\" have: \"%s\"\n",
		    want, VSB_data(vsb));
		WRONG("expectation");
	}
	VSB_fini(vsb);

	junquot(want, from);
}

static void
notutf(const char *p, unsigned off)
{
	struct vsb *vsb, vsbs[1];
	const char *err;
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AZ(vsbjascii(vsb, p, &err));
	AZ(VSB_finish(vsb));
	assert(err == p + off);
	VSB_fini(vsb);
}

static void
jasciioverflow(const char *p)
{
	struct vsb *vsb, vsbs[1];
	const char *err = NULL;
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AZ(vsbjascii(vsb, p, &err));
	AZ(err);
	AN(VSB_finish(vsb));
	VSB_fini(vsb);
}

static void
t_vsbjascii(void)
{
	jascii("", "");
	jascii("abc", "abc");
	jascii("\"", "\\\"");
	jascii("a\"b", "a\\\"b");
	jascii("\t", "\\t");
	jascii("\tbcde", "\\tbcde");
	jascii("\x01", "\\u0001");
	jascii("xx\x01", "xx\\u0001");
	jascii("xx€Z", "xx\\u20acZ");
	jascii("Z🐰", "Z\\ud83d\\udc30");

	jascii("ࠀ", "\\u0800");
	jascii("퟿", "\\ud7ff");
	jascii("", "\\ue000");
	jascii("abࠀ", "ab\\u0800");
	jascii("ab퟿", "ab\\ud7ff");
	jascii("ab", "ab\\ue000");
	jascii("ࠀcd", "\\u0800cd");
	jascii("퟿cd", "\\ud7ffcd");
	jascii("cd", "\\ue000cd");
	// highest code point 0x10ffff
	jascii("\xf4\x8f\xbf\xbf","\\udbff\\udfff");

	notutf("\x81", 0);
	notutf("x\x81", 1);

	jascii("🐰€ ", "\\ud83d\\udc30\\u20ac ");
	jasciioverflow("🐰€  ");
	jasciioverflow("€  🐰");
	jasciioverflow("🐰  €");
	jasciioverflow("🐰€\t");
}

static void
jminimal(const char *from, const char *want)
{
	struct vsb *vsb, vsbs[1];
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AN(vsbjminimal(vsb, from));
	AZ(VSB_finish(vsb));
	if (strcmp(want, VSB_data(vsb))) {
		printf("want: \"%s\" have: \"%s\"\n",
		    want, VSB_data(vsb));
		WRONG("expectation");
	}
	VSB_fini(vsb);

	junquot(want, from);
}

static void
jminimaloverflow(const char *p)
{
	struct vsb *vsb, vsbs[1];
	char buf[20];

	vsb = VSB_init(vsbs, buf, sizeof buf);
	assert(vsb != NULL);
	AZ(vsbjminimal(vsb, p));
	AN(VSB_finish(vsb));
	VSB_fini(vsb);
}

static void
t_vsbjminimal(void)
{
	jminimal("", "");
	jminimal("abc", "abc");
	jminimal("\"", "\\\"");
	jminimal("a\"b", "a\\\"b");
	jminimal("\t", "\\t");
	jminimal("\tbcde", "\\tbcde");
	jminimal("\x01", "\\u0001");
	jminimal("xx\x01", "xx\\u0001");
	jminimal("xx€\\€Z", "xx€\\\\€Z");
	jminimal("Z🐰", "Z🐰");

	jminimal("🐰🐰🐰🐰€", "🐰🐰🐰🐰€");
	jminimaloverflow("🐰🐰🐰🐰€ ");
	jminimaloverflow("🐰🐰🐰🐰€\t");
	jminimaloverflow("🐰🐰🐰🐰🐰");
}

static void
t_vsbjunquot(void)
{
	junquot("", "");
	junquot("\\t€", "\t€");
	junquot("\\u0022", "\"");
	junquot("\\u0800X", "ࠀX");
	junquot("\\ue000X", "X");

	junquoterr("\\x", 1);
	junquoterr("\\u", 2);
	junquoterr("\\u1", 3);
	junquoterr("\\u12", 4);
	junquoterr("\\u123", 5);

	junquoterr("\\ud801", 6);
	junquoterr("\\ud801\\", 7);
	junquoterr("\\ud801\\u", 8);
	junquoterr("\\ud801\\u1", 9);
	junquoterr("\\ud801\\u12", 10);
	junquoterr("\\ud801\\u123", 11);
	junquoterr("\\ud801\\u1234", 12);

	// low surrogate
	junquoterr("\\udc00", 6);

#define B "\\ud83d\\udc30"	// 🐰
#define E "\\u20ac"		// €
	junquot(B B B B E, "🐰🐰🐰🐰€");
	junquotoverflow(B B B B E " ");
	junquotoverflow(B B B B "    ");
	junquotoverflow(B B B B E "\\t");
	junquotoverflow(B B B B E "\\u0020");
	junquotoverflow(B B B B E "\\u07ff");
	junquotoverflow(" " B B B B E);
	junquotoverflow(" " B B B E B);

#undef B
#undef E
}

int
main(void)
{
	t_is_jquot();
	t_vsbjascii();
	t_vsbjminimal();
	t_vsbjunquot();
	return (0);
}
