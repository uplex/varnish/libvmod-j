=========================================
A JSON formatter for VCL which sucks less
=========================================

.. role:: ref(emphasis)

.. _Varnish-Cache: https://varnish-cache.org/

This project provides JSON formatting facilities for `Varnish-Cache`_
VCL as a module (VMOD).

PROJECT RESOURCES
=================

* The primary repository is at https://code.uplex.de/uplex-varnish/libvmod-j

  This server does not accept user registrations, so please use ...

* the mirror at https://gitlab.com/uplex/varnish/libvmod-j for issues,
  merge requests and all other interactions.

.. _Höhrmann UTF-8 decoder: http://bjoern.hoehrmann.de/utf-8/decoder/dfa/

.. _Exhaustive Test Program: https://git.sr.ht/~slink/hoehrmann-utf8

This project contains, as a submodule, the `Höhrmann UTF-8 decoder`_
as tested by the `Exhaustive Test Program`_.

INTRODUCTION
============

**BEFORE USING THIS VMOD, DO READ THIS DOCUMENTATION AND IN PARTICULAR
THE WARNING IN** `vmod_j(3) <src/vmod_j.man.rst>`_.

.. _JSON: https://www.json.org/json-en.html

Formatting `JSON`_ in pure VCL is a PITA, because string processing in
VCL was never made for it. VCL being a Domain Specific Language, it
was made for processing HTTP headers.

Consider this simple example of a JSON object with a single key
``key``, for which the value ``value`` is to be taken from a VCL
header variable::

  {"key":"value"}

in pure VCL, you have to write something like this::

  {"{"key":""} + req.http.value + {"""} + "}"

Applause if you do not lose your mental sanity trying to understand
what this does.

This vmod has been written because it drove the author crazy to
maintain VCL code with constructs like the above (and that's an
exceptionally trivial case).

DESCRIPTION (Excerpt)
=====================

(The full version is in `vmod_j(3) <src/vmod_j.man.rst>`_)

With VMOD *j*, the example above looks like this::

  j.object("key" + req.http.value)

Things get more interesting with more complex data
structures. Consider this toy example::

    j.object(
	j.str("A") + j.null() +
	"B" + j.object(
	    "BB" + j.number(string="42.42e42") +
	    "CC" + false +
	    "DD" + true) +
	"C" + j.array("A" + 2 + j.object(j.nil()))
    )

The JSON produced by this code looks like this when reformatted with
:ref:`jq(1)`::

	{
	  "A": null,
	  "B": {
	    "BB": 4.242e+43,
	    "CC": false,
	    "DD": true
	  },
	  "C": [
	    "A",
	    2,
	    {}
	  ]
	}

Continue reading the documentation in `vmod_j(3) <src/vmod_j.man.rst>`_.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

SUPPORT
=======

.. _gitlab.com issues: https://gitlab.com/uplex/varnish/libvmod-j/-/issues

To report bugs, use `gitlab.com issues`_.

For enquiries about professional service and support, please contact
info@uplex.de\ .

CONTRIBUTING
============

.. _merge requests on gitlab.com: https://gitlab.com/uplex/varnish/libvmod-j/-/merge_requests

To contribute to the project, please use `merge requests on gitlab.com`_.

To support the project's development and maintenance, there are
several options:

.. _paypal: https://www.paypal.com/donate/?hosted_button_id=BTA6YE2H5VSXA

.. _github sponsor: https://github.com/sponsors/nigoroll

* Donate money through `paypal`_. If you wish to receive a commercial
  invoice, please add your details (address, email, any requirements
  on the invoice text) to the message sent with your donation.

* Become a `github sponsor`_.

* Contact info@uplex.de to receive a commercial invoice for SWIFT payment.

COPYRIGHT
=========

::

  Copyright 2023 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Author: Nils Goroll <nils.goroll@uplex.de>
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.
